# CIID Sublime Text (4) Color Scheme

Variations: Primary, Dark, and Inverted

The inverted scheme takes each color in the primary scheme and inverts RGB values.
Was too dark and high contrast so the dark theme was implemented.
The Dark theme 

The root `sublime-color-scheme` files contain the variables used to map color values.
root/
    - `CIID.sublime-color-scheme`
    - `CIID-dark.sublime-color-scheme`
    - `CIID-inverted.sublime-color-scheme`

The subdirectories contain extra definitions for various languages.
The `root/base/` dir files define a minimum set of scopes.

The php script simply copies the primary file in each dir to dark and inverted scheme files.
these are identical within each dir, they are for subl to use for each scheme and give variations by referencing the color values in the root files.


CIID, CIID-dark, CIID-inverted
/, /base, ,
