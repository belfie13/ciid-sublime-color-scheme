<?php

/**
 * Updates inverted and dark schemes from CIID.sublime-color-scheme files.
 */

declare(strict_types=1);

#update CIID-dark.sublime-color-scheme ⚠️ "rules" only


//############################################################################//

// copy sub dir files to dark/inverted variants "CIID-dark/inverted.sublime-color-scheme"

$directories = [
    __DIR__.'/base',
    __DIR__.'/B13Markup',
    __DIR__.'/CSS',
    __DIR__.'/HTML',
    __DIR__.'/Markdown',
    __DIR__.'/PHP',
    __DIR__.'/RegularExpressions',
    __DIR__.'/sublime-syntax',
 ];

foreach ($directories as $dirpath)
 {
    $color_scheme = file_get_contents($dirpath.'/CIID.sublime-color-scheme');
    file_put_contents($dirpath.'/CIID-dark.sublime-color-scheme', $color_scheme);
    file_put_contents($dirpath.'/CIID-inverted.sublime-color-scheme', $color_scheme);
 }


//############################################################################//

function invertSublimeColorScheme($scheme)
 {

    // echo $scheme;
    $ret = preg_replace_callback(
        // '/.*#\h{6}/SUis', 
        '/\#[0-9a-fA-F]{6}/', 
        function (array $matches):string
         {
            $hex = $matches[0];
            $newHex = '#';

            // ~~~>>> RED <<<~~~ //
            $r = $hex[1].$hex[2];
            $decr = 255 - hexdec($r);
            $hexr = dechex($decr);
            if ($decr < 16)
             {
                $newHex .= '0';
             }
            $newHex .= $hexr;

            // ~~~>>> GREEN <<<~~~ //
            $g = $hex[3].$hex[4];
            $decg = 255 - hexdec($g);
            $hexg = dechex($decg);
            if ($decg < 16)
             {
                $newHex .= '0';
             }
            $newHex .= $hexg;

            // ~~~>>> BLUE <<<~~~ //
            $b = $hex[5].$hex[6];
            $decb = 255 - hexdec($b);
            $hexb = dechex($decb);
            if ($decb < 16)
             {
                $newHex .= '0';
             }
            $newHex .= $hexb;

            return $newHex;
         },
        $scheme, -1, $count
     );


    return $ret;
 }

$color_scheme = file_get_contents(__DIR__.'/CIID.sublime-color-scheme');
$inverse = invertSublimeColorScheme($color_scheme);
file_put_contents($dirpath.'/CIID-inverted.sublime-color-scheme', $inverse);

if (preg_last_error() !== PREG_NO_ERROR)
 {
    echo preg_last_error_msg();
 }
