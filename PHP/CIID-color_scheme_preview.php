<?php

/**
 * 
 */

declare(strict_types=1);

namespace B13\PHP;

use Stringable;
use B13\Html\Element;
use B13\Html\Attr as Attr;
use B13\Html\{Text,Comment};

$variable = 'value';
$variable = 0.123456789;
const CONSTANT = 0x452;
const OTHER = 0b100101011;
const OCTAL = 0o341580;
$nulla = null;
$bfalse = false;
$btrue = true;
interface MyInterface
 {
    public function myPublicFunction(): mixed;
    public static function myPublicStaticFunction(): mixed;
 }
interface MyOtherInterface extends MyInterface
 {}
trait MyTrait
 {}
trait MyOtherTrait
 {
    use MyTrait;
    public function myPublicFunction(): mixed
     {}
    public static function myPublicStaticFunction(): mixed
     {}
 }
enum MyEnum
 {
    case Default;
    case One;
    case Two;
    public function myPublicFunction(): mixed
     {}
    public static function myPublicStaticFunction(): mixed
     {}
 }
enum MyIntEnum: int
 {
    case Default = 0;
    case One = 1;
    case Two = 2;
 }
enum MyStringEnum: string
 {
    case Default = 'Default';
    case One = 'One';
    case Two = 'Two';
 }
/**
 * @see https://example.com
 */
class MyClass
 {
    public function myPublicFunction(): mixed
     {}
    public static function myPublicStaticFunction(): mixed
     {}
 }
class MyOtherClass extends MyClass
 {
 }
class MyImplementsClass implements MyInterface
 {
 }
class MyMultiClass extends MyClass implements MyOtherInterface
 {
 }
class MyTraitClass
 {
    use MyOtherTrait;
 }
